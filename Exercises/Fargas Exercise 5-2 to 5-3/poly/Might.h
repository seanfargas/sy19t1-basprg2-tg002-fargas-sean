#pragma once
#include "Buff.h"
class Might :
	public Buff
{
public:
	Might(int might);
	~Might();

private:
	int mMight;
};

