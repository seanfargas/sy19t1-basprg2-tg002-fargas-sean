#pragma once
#include "Buff.h"
class Heal :
	public Buff
{
public:
	Heal(int hp);
	~Heal();

	string type();

private:
	int mHp;
};

