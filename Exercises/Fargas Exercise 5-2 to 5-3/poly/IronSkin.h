#pragma once
#include "Buff.h"
class IronSkin :
	public Buff
{
public:
	IronSkin(int ironskin);
	~IronSkin();

private:
	int mIronSkin;
};

