#pragma once
#include "Buff.h"
class Haste :
	public Buff
{
public:
	Haste(int haste);
	~Haste();

private:
	int mHaste;
};

