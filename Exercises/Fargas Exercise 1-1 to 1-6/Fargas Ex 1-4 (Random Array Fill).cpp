#include <iostream>
#include <conio.h>
#include <time.h>

using namespace std;

int main()
{
	int random[10];

	srand(time(NULL));

	for (int i = 0; i < 10; i++) {
		random[i] = (rand() % 100) + 1;

		cout << random[i] << endl;
	}
	_getch();
	return 0;
}