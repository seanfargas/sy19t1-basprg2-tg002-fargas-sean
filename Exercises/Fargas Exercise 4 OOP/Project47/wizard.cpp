#include "Wizard.h"
#include "Spell.h"

Wizard::Wizard()
{
	this->name = "";
	this->hp = 0;
	this->mp = 0;
}

Wizard::Wizard(string name, int hp, int mp, Spell*spell)
{
	this->name = name;
	this->hp = hp;
	this->mp = mp;
	this->spell = spell;
}

void Wizard::attack(Wizard*player,Wizard*enemy)
{
	cout << endl;
	cout << player->name << " used " << player->spell->name << " on " << enemy->name << endl;
	enemy->hp -= player->spell->damage;
	player->mp -= spell->mpCost;
}

void Wizard::displayCurrent()
{
	cout << this->name << "'s Health: " << this->hp << endl;

	cout << this->name << "'s Mana: " << this->mp << endl;
}

