#include <string>
#include <iostream>
#include "Spell.h"
#include "Wizard.h"

using namespace std;

int main()
{
	Spell* fireball = new Spell("Fireball", 5, 25);

	Wizard* playerOne = new Wizard("Harry Potter", 100, 50, fireball);
	Wizard* playerTwo = new Wizard("Voldemort", 100, 50, fireball);

	playerOne->attack(playerOne, playerTwo);
	playerTwo->attack(playerTwo, playerOne);

	playerOne->displayCurrent();
	playerTwo->displayCurrent();

	system("Pause");

	return 0;
}