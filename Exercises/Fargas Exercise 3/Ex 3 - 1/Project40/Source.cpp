#include <iostream>
#include <time.h>

using namespace std;

/*Create a function which accepts an integer array in the form of a pointer as a parameter to the function. 
	This function will fill the array with random values.

Use int* numbers instead of int numbers[]*/

void integerArray(int* numbers[],int& size)
{
	int arr[10];
	for (int j = 0; j < size; j++)
	{
		arr[j] = rand() % 9;
	}
	numbers[10] = arr[10];

	cout << numbers[10] << endl;
}

int main()
{
	int size = 10;
	int numbers[size];

	integerArray(numbers, size);

	system("pause");
	return 0;

}

