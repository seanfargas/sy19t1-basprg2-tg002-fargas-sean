#include <iostream>

using namespace std;

void generateArray(int *a, int size)
{
	for (int j = 0; j < size; j++)
	{
		a[j] = rand() % 9;
	}

	cout << a[0], a[1], a[2], a[3], a[4];
	cout << endl;
}

int main()
{
	const int size = 5;
	int a[size];

	generateArray(a, size);

	system("pause");
	return 0;
}