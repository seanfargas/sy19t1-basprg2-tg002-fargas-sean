/*
- The player starts with 1000 gold.
- The player bets gold that is not zero or greater than current gold.
- AI rolls the 2 dice. The sum of the 2 dice will be the value of his roll.
- Player rolls the 2 dice. If the player rolls a higher value, he wins his bet.
- If player rolls snake eyes (1-1), he receives thrice his bet.
- If both the player and the AI rolls the same value, it�s a draw. This is also applicable with Snake Eyes.
- The game will only end if the player loses all his gold.

Ex 2-1 (Bet)
Create a function that asks the player for his bet and deducts the user�s gold immediately. This function must accept a reference.

Create 2 versions of this function:
A function that returns the bet value and also deducts the user�s gold.
Same as the first function but it must return void

NOTE: Explain the difference of the 3 functions:
int bet(int gold) // You cannot deduct player�s gold. Function is solely used for determining bet
int bet(int& gold) // You can deduct player�s gold and return the bet as normal variable.
void bet(int& gold, int& bet) // You can deduct player�s gold and bet will be set via referencing.

Ex 2-2 (Dice Roll)
Create a function which rolls the 2 dice. This should work for both the player and AI. Since there are 2 dice, they must use reference as parameter.

Ex 2-3 (Payout)
Create a function which evaluates how much the player should win/lose depending on the outcome of the dice roll. Use reference to modify the player�s gold.

Ex 2-4 (Play Round)
Create a playRound function to remove duplicate code. This function will be continuously called in the main function until player loses all his gold. This function must accept the player�s gold as reference.
*/

#include <iostream>
#include <conio.h>

using namespace std;

void playerFBet(int& playerBet, int& playerGold)
{
	int pGold = 1000;
	cout << "Place your bet." << endl;
	cin >> playerBet;
	playerGold -= playerBet;
}

void aiFBet(int& aiBet, int& aiGold)
{
	int aGold = 1000;
	aiBet = rand() % 1000 + 1;
	aiGold -= aiBet;
}

void diceRoll(int playerDice, int aiDice)
{
	int playerDice1, playerDice2, aiDice1, aiDice2;

	playerDice1 = rand() % 6 + 1;
	playerDice2 = rand() % 6 + 1;
	aiDice1 = rand() % 6 + 1;
	aiDice2 = rand() % 6 + 1;

	playerDice = playerDice1 + playerDice2;
	aiDice = aiDice1 + aiDice2;

}

void payout(int playerDice, int aiDice, int playerGold, int playerBet, int aiGold, int aiBet)
{
	if (playerDice > aiDice)
	{
		playerGold += aiBet;
		aiBet -= aiGold;

		cout << "Player has gained " << aiBet << endl;
	}
	else if (playerDice < aiDice)
	{
		aiGold += playerBet;
		playerBet -= playerGold;

		cout << "Player has lost " << playerBet << endl;
	}
}

void playRound(int& playerRound, int playerGold)
{
	if (playerGold >= 1)
	{
		playerRound = 0;
	}
	else if (playerGold <= 0)
	{
		playerRound = 1;
	}
}

int main()
{
	int playerRound;

	while (playerRound = 0)
	{
		cout << "Welcome player!" << endl;
		cout << "Your starting balance is 1000 gold." << endl;
		cout << "You get the first turn." << endl;

		playerFBet;

		cout << "AI's turn." << endl;

		aiFBet;

		cout << "Dice roll commense..." << endl;

		diceRoll;

		cout << "Payout..." << endl;

		payout;

		playerRound;

		if (playerRound = 1)
		{
			system("pause");
			return 0;
		}
		else if (playerRound = 0)
		{
			system("pause");
			system("CLS");
		}

	}

}