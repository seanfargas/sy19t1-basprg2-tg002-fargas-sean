#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

void packagefunction()
{	
	int temp;
	int packages[] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };

	for (int i = 0; i < 7; i++)
	{
		for (int y = i + 1; y < 7; y++)
		{
			if (packages[i] > packages[y])
			{
				temp = packages[i];
				packages[i] = packages[y];
				packages[y] = temp;
			}
		}
	}

	for (int i = 0; i < 7; i++)
	{
		cout << "Package#" << i << ":" << packages[i] << endl;
	}
}

int main()
{
	int cgold = 250;
	int pgold = 0;

	packagefunction();

	while (true)
	{
		int shopping = 1;

		cout << endl;
		cout << "Current Gold: " << cgold << endl;
		cout << "Input price of the item to buy..." << endl;
		cin >> pgold;
		cout << endl;
		cgold = cgold - pgold;
		cout << "You have purchased the item!" << endl;

		_getch();
		system("CLS");

		cout << "Continue shopping?" << endl;
		cout << "Yes: (1)";
		cout << "No: (0)";
		cin >> shopping;

		_getch();
		system("CLS");

		if (shopping == 1)
		{
			if (cgold >= 30751)
			{
				cout << "You do not have enough money" << endl;
				cout << "We suggest you get package(s):" << endl;
				cout << endl;
				packagefunction();
			}
			else if (cgold >= 30750)
			{
				cout << "You do not have enough money" << endl;
				cout << "We suggest you get package(s): 0, 1, 2, 3, 4, 5" << endl;
				cout << endl;
				packagefunction();
			}
			else if (cgold >= 13333)
			{
				cout << "You do not have enough money" << endl;
				cout << "We suggest you get package(s): 0, 1, 2, 3, 4" << endl;
				cout << endl;
				packagefunction();
			}
			else if (cgold >= 4050)
			{
				cout << "You do not have enough money" << endl;
				cout << "We suggest you get package(s): 0, 1, 2, 3" << endl;
				cout << endl;
				packagefunction();
			}
			else if (cgold >= 1780)
			{
				cout << "You do not have enough money" << endl;
				cout << "We suggest you get package(s): 0, 1, 2" << endl;
				cout << endl;
				packagefunction();
			}
			else if (cgold >= 500)
			{
				cout << "You do not have enough money" << endl;
				cout << "We suggest you get package(s): 0, 1" << endl;
				cout << endl;
				packagefunction();
			}
			else if (cgold <= 499)
			{
				cout << "You do not have enough money" << endl;
				cout << "We suggest you get package(s): 0" << endl;
				cout << endl;
				packagefunction();
			}
		}
		else if (shopping == 0)
		{
			_getch();
			return 0;
		}
	}

	_getch();
	return 0;

}
