#include <iostream>
#include <vector>
#include <conio.h>
#include <string>
#include "Warrior.h"
#include "Assassin.h"
#include "Mage.h"

using namespace std;

int main()
{
	int playerName;
	int answer;

	Warrior* player1 = new Warrior("MANOY", "Warrior", 25, 10, 15, 5, 20); // (string name, string class, hp, mp, pow, agi, dex , vit)
	Assassin* player2 = new Assassin( "JERICHO",   "Assassin", 25, 10, 15, 5, 20);
	Mage* player3 = new Mage("ION", "Mage", 25, 10, 15, 5, 20); 
	

	cout << "input your name: ";
	cin >>playerName;

	system("pause");
	system("CLS");

	cout << "Choose a class: " << endl;
	cout << "               [1] Warrior" << endl;
	cout << "               [2] Assassin" << endl;
	cout << "               [3] Mage" << endl;
	cin >> answer;

	system("pause");
	system("CLS");
	if (answer == 1)
	{
		cout << "Name: " << player1->getName() << endl; // getname()
		cout << "Class: " << player1->getClasses() << endl; //getclass()
		cout << "HP: " << player1->getHp() << endl; //gethp()
		cout << "Pow: " << player1 -> getPow() << endl; // getpow()
		cout << "Dex: " << player1 ->getDex() << endl; //getDex()
		cout << "Agi: " << player1 ->getAgi() << endl; //getAgi()

		_getch();
	}
	if (answer == 2)
	{
		cout << "Name: " << player2->getName() << endl; // getname()
		cout << "Class: " << player2->getClasses() << endl; //getclass()
		cout << "HP: " << player2->getHp() << endl; //gethp()
		cout << "Pow: " << player2->getPow() << endl; // getpow()
		cout << "Vit: " << player2->getVit() << endl;// getVit()             note: they all have different classes 
		cout << "Dex: " << player2->getDex() << endl; //getDex()
		cout << "Agi: " << player2->getAgi() << endl; //getAgi()
		_getch();
	}
	if (answer == 3)
	{
		cout << "Name: " << player3->getName() << endl; // getname()
		cout << "Class: " << player3->getClasses() << endl; //getclass()
		cout << "HP: " << player3->getHp() << endl; //gethp()
		cout << "Pow: " << player3->getPow() << endl; // getpow()
		cout << "Vit: " << player3->getVit() << endl;// getVit()             note: they all have different classes 
		cout << "Dex: " << player3->getDex() << endl; //getDex()
		cout << "Agi: " << player3->getAgi() << endl; //getAgi()
		_getch();
	}
	cout << endl;
	//cout << "VS" << endl;
	//cout << endl;
	//cout << "Name: " << enemyname << endl;
	//cout << "Class: " << enemyclass << endl;
	//cout << "HP: " << enemyhp << endl;
	//cout << "Pow: " << enemypow << endl;
	//cout << "Vit: " << enemyvit << endl;
	//cout << "Dex: " << enemydex << endl;
	//cout << "Agi: " << enemyagi << endl;

	system("Pause");

	return 0;

}
