#include "Warrior.h"





Warrior::Warrior(string name, string classes, int hp, int pow, int vit, int agi, int dex)
{
	mName = name;
	mClasses = classes;
	mHp = hp;
	mPow = pow;
	mVit = vit;
	mAgi = agi;
	mDex = dex;
}

Warrior::~Warrior()
{
}

string Warrior::getName()
{
	return this->mName;
}

string Warrior::getClasses()
{
	return this->mClasses;
}

int Warrior::getHp()
{
	return this->mHp;
}

int Warrior::getPow()
{
	return this->mPow;
}

int Warrior::getVit()
{
	return this->mVit;
}

int Warrior::getAgi()
{
	return this->mAgi;
}

int Warrior::getDex()
{
	return this->mDex;
}
