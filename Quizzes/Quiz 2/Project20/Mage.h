#pragma once
#include <string>

using namespace std;

class Mage
{
public:
	Mage(string name, string classes, int hp, int pow, int vit, int agi, int dex);
	~Mage();

	string getName();
	void setName(string name);
	void takeDamage(int damage);

	string getClasses();
	int getHp();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();


private:

	string mClasses;
	string mName;
	int mHp;
	int mPow;
	int mVit;
	int mAgi;
	int mDex;
};

