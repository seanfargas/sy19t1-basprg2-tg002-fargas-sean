#include "Assassin.h"




Assassin::Assassin(string name, string classes, int hp, int pow, int vit, int agi, int dex)
{
	mName = name;
	mClasses = classes;
	mHp = hp;
	mPow = pow;
	mVit = vit;
	mAgi = agi;
	mDex = dex;
}

Assassin::~Assassin()
{
}

string Assassin::getName()
{
	return this->mName;
}

string Assassin::getClasses()
{
	return this->mClasses;
}

int Assassin::getHp()
{
	return this->mHp;
}

int Assassin::getPow()
{
	return this->mPow;
}

int Assassin::getVit()
{
	return this->mVit;
}

int Assassin::getAgi()
{
	return this->mAgi;
}

int Assassin::getDex()
{
	return this->mDex;
}
