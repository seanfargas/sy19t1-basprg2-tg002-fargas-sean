#pragma once
#include <string>

using namespace std;
class Assassin
{
public:
	Assassin(string name, string classes, int hp, int pow, int vit, int agi, int dex);
	~Assassin();

	string getName();
	string getClasses();
	int getHp();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();
private:

	string mClasses;
	string mName;
	int mHp;
	int mPow;
	int mVit;
	int mAgi;
	int mDex;
};

