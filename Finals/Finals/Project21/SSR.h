#pragma once
#include "Item.h"

class SSR :
	public Item
{
public:
	SSR(string name, int rarity);
	~SSR();

	virtual void rarityFx(Character*player);
};

