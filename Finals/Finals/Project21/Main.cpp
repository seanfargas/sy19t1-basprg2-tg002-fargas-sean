#include <iostream>
#include <string>
#include "Character.h"	
#include "Item.h"
#include "SSR.h"
#include "SR.h"
#include "R.h"
#include "HP.h"
#include "Bomb.h"
#include "Crystal.h"

using namespace std;

int main()
{
	Character * player = new Character("Rodil", 100, 0, 100);

	while (true)
	{
		player->display();
		player->flow();
		system("pause");
		system("CLS");
	}
	return 0;
}