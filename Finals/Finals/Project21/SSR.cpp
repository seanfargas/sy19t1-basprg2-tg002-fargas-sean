#include "SSR.h"
#include <iostream>

using namespace std;

SSR::SSR(string name, int rarity) : Item (name)
{
	rarity = rarity;
}

SSR::~SSR()
{
}

void SSR::rarityFx(Character* player)
{
	cout << "You pulled SSR!" << endl;
	cout << "You recieve 50 Rarity Points. " << endl;
	player->addSSR();
}
