#pragma once
#include <string>
#include <vector>

using namespace std;
class Item;
class Character
{
public:
	Character(string name, int hp, int totalR, int totalCrystal);
	~Character();

	vector <Item*> items;

	string getName();
	int getHp();
	int getTotalR();
	int getTotalCrystal();

	void bomb();
	void heal();
	void addCrystal();
	void minusCrystal();
	void addR();
	void addSR();
	void addSSR();

	bool alive();
	void flow();
	void display();

private:
	string mName;
	int mHp;
	int mTotalCrystal;
	int mTotalR;
};

