#include "SR.h"
#include <iostream>

using namespace std;

SR::SR(string name, int rarity) : Item(name)
{
	rarity = rarity;
}

SR::~SR()
{
}

void SR::rarityFx(Character*player)
{
	cout << "You pulled SSR!" << endl;
	cout << "You recieve 10 Rarity Points. " << endl;
	player->addSR();
}
