#pragma once
#include "Item.h"
class HP :
	public Item
{
public:
	HP(string name, int rarity);
	~HP();

	virtual void healFx(Character*player);
};

