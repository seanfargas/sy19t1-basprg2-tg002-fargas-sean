#include "HP.h"
#include <iostream>

using namespace std;

HP::HP(string name, int rarity) : Item(name)
{
	rarity = rarity;
}

HP::~HP()
{
}

void HP::healFx(Character * player)
{
	cout << "You have pulled a Health Potion!" << endl;
	cout << "You have receieved 30 HP. " << endl;
	player->heal();
}
