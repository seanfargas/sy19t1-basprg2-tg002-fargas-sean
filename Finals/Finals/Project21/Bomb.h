#pragma once
#include "Item.h"
class Bomb :
	public Item
{
public:
	Bomb(string name, int rarity);
	~Bomb();

	virtual void bombFx(Character*player);
};

