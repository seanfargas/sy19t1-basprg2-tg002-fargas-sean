#pragma once
#include "Item.h"
#include <iostream>

using namespace std;

class Crystal :
	public Item
{
public:
	Crystal(string name, int rarityPt);
	~Crystal();

	virtual void bombFx(Character*player);
};

