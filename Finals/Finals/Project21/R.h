#pragma once
#include "Item.h"

class R :
	public Item
{
public:
	R(string name, int rarity);
	~R();

	virtual void rarityFx(Character* player);
};

