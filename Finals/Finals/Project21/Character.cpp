#include "Character.h"
#include <iostream>
#include "Item.h"
#include "HP.h"
#include "Bomb.h"
#include "Crystal.h"
#include "R.h"
#include "SR.h"
#include "SSR.h"


	
Character::Character(string name, int hp, int totalR, int totalCrystal)
{
	mName = name;
	mHp = hp;
	mTotalR = totalR;
	mTotalCrystal = totalCrystal;
	items.push_back(new SSR("SSR", 50));
	items.push_back(new SR("SR", 10));
	items.push_back(new R("R", 1));
	items.push_back(new HP("HP", 0));
	items.push_back(new Bomb("Bomb", 0));
	items.push_back(new Crystal("Crystal", 0));
}

Character::~Character()
{
}

string Character::getName()
{
	return mName;
}

int Character::getHp()
{
	return mHp;
}

int Character::getTotalR()
{
	return mTotalR;
}

int Character::getTotalCrystal()
{
	return mTotalCrystal;
}

void Character::bomb()
{
	this->mHp = this->mHp - 25;
}

void Character::heal()
{
	this->mHp = this->mHp + 30;
}

void Character::addCrystal()
{
	this->mTotalCrystal = this->mTotalCrystal + 15;
}

void Character::minusCrystal()
{
	this->mTotalCrystal = this->mTotalCrystal - 5;
}

void Character::addR()
{
	this->mTotalR = this->mTotalR + 1;
}

void Character::addSR()
{
	this->mTotalR = this->mTotalR + 10;
}

void Character::addSSR()
{
	this->mTotalR = this->mTotalR + 50;
}




bool Character::alive()
{
	return mHp > 0;
}

void Character::flow()
{
	int randItems = rand() + 100;
	this->minusCrystal();

	if (randItems <= 1)
	{
		this->items[0];
	}
	else if (randItems > 2 && randItems <= 9)
	{
		this->items[1];
	}
	else if (randItems > 10 && randItems <= 40)
	{
		this->items[2];
	}
	else if (randItems > 41 && randItems <= 55)
	{
		this->items[3];
	}
	else if (randItems > 56 && randItems <= 75)
	{
		this->items[4];
	}
	else if (randItems > 76 && randItems <= 100)
	{
		this->items[5];
	}
}

void Character::display()
{
	cout << "Character: " << mName << endl;
	cout << "HP: " << this->mHp << endl;
	cout << "Crystals: " << this->mTotalCrystal << endl;
	cout << "Rarity Points: " << this->mTotalR << endl;
}

