#include "R.h"
#include <iostream>

using namespace std;

R::R(string name, int rarity) : Item(name)
{
	rarity = rarity;
}

R::~R()
{
}

void R::rarityFx(Character*player)
{
	cout << "You pulled SSR!" << endl;
	cout << "You recieve 50 Rarity Points. " << endl;
	player->addR();
}
