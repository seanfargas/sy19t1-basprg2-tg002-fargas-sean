#pragma once
#include "Item.h"

class SR :
	public Item
{
public:
	SR(string name, int rarity);
	~SR();

	virtual void rarityFx(Character* player);
};

