#include "Bomb.h"
#include <iostream>

using namespace std;

Bomb::Bomb(string name, int rarity) : Item(name)
{
	rarity = rarity;
}

Bomb::~Bomb()
{
}

void Bomb::bombFx(Character * player)
{
	cout << "Oh no, You have pulled out a Bomb!" << endl;
	cout << "You recieve 25 damage." << endl;
	player->bomb();
}
